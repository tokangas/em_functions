function [ wave, units ] = wave_variables( f, epsilon_r, mu_r )
%wave_variables Return common electromagnetics plane wave parametres in a struct.
%   Detailed explanation goes here
    
%% Handle non mandatory parametres
    if nargin == 0
        f = 1;
    end
    if nargin < 3
        mu_r = ones(size(f));
    end
    if nargin < 2
        epsilon_r = ones(size(f));
    end
    
%% Scale parametre matrices to match in size
    
    % http://se.mathworks.com/matlabcentral/answers/25317#answer_33224 
    if ~(isequal(size(mu_r), size(f)) || (isvector(mu_r) && isvector(f) && numel(mu_r) == numel(f)))
        mu_r = repmat(mu_r(1,1,1), size(f));
    end
    if ~(isequal(size(epsilon_r), size(f)) || (isvector(epsilon_r) && isvector(f) && numel(epsilon_r) == numel(f)))
        epsilon_r = repmat(epsilon_r(1,1,1), size(f));
    end
    
%% Calculate wave variables
    EM = em_constants();
    
    wave = struct( ...
        'epsilon',      EM.epsilon_0*epsilon_r,             ...
        'mu',           EM.mu_0*mu_r,                       ...
        'omega',        2*pi*f,                             ...
        'lambda_0',     EM.c_0./f,                          ...
        'lambda',       abs(1./(f.*sqrt(EM.mu_0.*mu_r.*     ...
                        EM.epsilon_0.*epsilon_r))),         ...
        'k_0',          2*pi*f*sqrt(EM.mu_0*EM.epsilon_0),  ...
        'k',            2*pi*f.*sqrt(EM.mu_0.*mu_r.*        ...
                        EM.epsilon_0.*epsilon_r)            ...
        );
    units = struct( ...
        'epsilon',      'As/Vm',    ...
        'mu',           'Vs/Am',	...
        'omega',        'rad/s',	...
        'lambda_0',     'm',        ...
        'lambda',       'm',        ...
        'k_0',          '1/m',      ...
        'k',          '1/m'       ...
    );

end
