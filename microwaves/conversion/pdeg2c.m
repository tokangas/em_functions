function [ c ] = pdeg2c( a, d )
%pdeg2c Convert abs and angle in degrees to complex number.
%   Detailed explanation goes here
    c = a.*exp(1j.*d./180.*pi);
end

