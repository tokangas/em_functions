function [ S ] = abcd2s( ABCD, Z_0 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        Z_0 = 50;
    end
    
    A = ABCD(1,1); B = ABCD(1,2);
    C = ABCD(2,1); D = ABCD(2,2);
    
    denom = A + B/Z_0 + C*Z_0 + D;
    
    S = [(A + B/Z_0 - C*Z_0 - D), (2*(A*D-B*C));...
         (2), (-A + B/Z_0 - C*Z_0 + D)]./denom;
end