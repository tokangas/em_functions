function [ Gamma ] = z2gamma( Z, Z_0 )
%z2gamma Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        Z_0 = 50;
    end
    Gamma = (Z - Z_0)./(Z + Z_0);
end

