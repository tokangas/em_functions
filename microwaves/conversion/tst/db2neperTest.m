%% db2neper
a = [1 2 3; 4 5 6; 7 8 9];
assert(sum(sum(db2neper(a) - a./8.6859)) < 1e-3);