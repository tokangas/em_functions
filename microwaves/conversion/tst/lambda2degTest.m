%% lambda2deg
assert(lambda2deg(0.25) - 90 < 1e-3);
assert(lambda2deg(0.50) - 180 < 1e-3);
assert(lambda2deg(0.125) - 45 < 1e-3);