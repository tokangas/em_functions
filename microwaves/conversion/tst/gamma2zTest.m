%% gamma2z
infinity = 1e100;
assert(gamma2z(0) - 50 < 1e-3);
assert(gamma2z(1) > infinity);
assert(gamma2z(-1) < 1e-3);