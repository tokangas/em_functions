%% c2pdeg
[a, d] = c2pdeg(0.707+0.707j);
assert(a - 1 < 1e-3);
assert(d - 45 < 1e-3);

[a, d] = c2pdeg(1j);
assert(a - 1 < 1e-3);
assert(d - 90 < 1e-3);
