%% pdeg2c
assert(abs(pdeg2c(1,45)-(0.707+0.707j)) < 1e-3);
assert(abs(pdeg2c(1,-45)-(0.707-0.707j)) < 1e-3);
assert(pdeg2c(1,90) - 1j < 1e-3);