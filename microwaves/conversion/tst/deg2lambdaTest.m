%% deg2lambda
assert(deg2lambda(90) - 0.25 < 1e-3);
assert(deg2lambda(180) - 0.50 < 1e-3);
assert(deg2lambda(45) - 0.125 < 1e-3);