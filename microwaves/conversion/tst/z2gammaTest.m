%% z2gamma
infinity = 1e100;
assert(z2gamma(50) < 1e-3);
assert(z2gamma(infinity) - (1) < 1e-3);
assert(z2gamma(0) - (-1) < 1e-3);