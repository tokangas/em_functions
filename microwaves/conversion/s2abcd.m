function [ ABCD ] = s2abcd( S, Z_0 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        Z_0 = 50;
    end
    
    denom = 2*S(2,1);
    
    ABCD = [((1 + S(1,1)) * (1 - S(2,2)) + S(1,2)*S(2,1)),...
        Z_0*((1 + S(1,1)) * (1 + S(2,2)) - S(1,2)*S(2,1));...
        ((1 - S(1,1)) * (1 - S(2,2)) - S(1,2)*S(2,1))/Z_0,...
        ((1 - S(1,1)) * (1 + S(2,2)) + S(1,2)*S(2,1))]./denom;
end

