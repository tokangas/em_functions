function [ Z_S ] = gamma2z( Gamma, Z_0 )
%gamma2z Refl. coefficient to load impedance.
    if nargin < 2
        Z_0 = 50;
    end
    
    Z_S = Z_0*(1+Gamma)/(1-Gamma);
end