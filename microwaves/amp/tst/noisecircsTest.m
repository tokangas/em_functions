%% noisecircsTest

%% Pozar, Microwave Engineering 4th edition: Example 12.5
F_min = 1.6;
Gamma_opt = 0.62*exp(1j*(100)/180*pi);
R_N = 20;
S = [0.6*exp(1j*(-60)/180*pi),0.05*exp(1j*(26)/180*pi);1.9*exp(1j*(81)/180*pi),0.5*exp(1j*(60)/180*pi)];

[c,r] = amplifier_constant_noise_circs(2.0, F_min, R_N, Gamma_opt);

assert(abs(c - pdeg2c(0.56,100)) < 1e-2);
assert(r - 0.24 < 1e-2);