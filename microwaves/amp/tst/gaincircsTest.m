%% gaincircsTest

%% Pozar, Microwave Engineering 4th edition: Example 12.4
[G_s_c, G_s_r] = amplifier_constant_gain_circs([2 3], pdeg2c(0.75,-120));
[G_l_c, G_l_r] = amplifier_constant_gain_circs([0 1], pdeg2c(0.6,-70));

assert(abs(G_s_c(1)-pdeg2c(0.627,120)) < 1e-2);
assert(G_s_r(1)-0.294 < 1e-2);

assert(abs(G_s_c(2)-pdeg2c(0.706,120)) < 1e-2);
assert(G_s_r(2)-0.166 < 1e-2);

assert(abs(G_l_c(1)-pdeg2c(0.440,70)) < 1e-2);
assert(G_l_r(1)-0.440 < 1e-2);

assert(abs(G_l_c(2)-pdeg2c(0.520,70)) < 1e-2);
assert(G_l_r(2)-0.303 < 1e-2);