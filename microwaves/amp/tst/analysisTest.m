%% AnalysisTest

%% Pozar, Microwave Engineering 4th edition: Example 12.1
Z_S = 25; % Ohm
Z_L = 40; % Ohm
S = [0.38*exp(1j*(-158)/180*pi),0.11*exp(1j*(54)/180*pi);3.5*exp(1j*(80)/180*pi),0.40*exp(1j*(-43)/180*pi)];
amp = amplifier_analysis(S, Z_S, Z_L);
assert(amp.Gamma_S - (-0.333) < 1e-3);
assert(amp.Gamma_L - (-0.111) < 1e-3);
assert(abs(amp.Gamma_in - pdeg2c(0.365,-152)) < 1e-3);
assert(abs(amp.Gamma_out - pdeg2c(0.545,-43)) < 1e-3);
assert(amp.G - 13.1 < 1e-1);
assert(amp.G_A - 19.8 < 1e-1);
assert(amp.G_T - 12.6 < 1e-1);

%% Pozar, Microwave Engineering 4th edition: Example 12.2 
S = [0.869*exp(1j*(-159)/180*pi),0.031*exp(1j*(-9)/180*pi);4.250*exp(1j*(61)/180*pi),0.507*exp(1j*(-117)/180*pi)];
amp = amplifier_analysis(S);
assert(amp.absDelta - 0.336 < 1e-3);
assert(amp.K - 0.383 < 1e-3);

%% Pozar, Microwave Engineering 4th edition: Example 12.3 
S = [pdeg2c([0.72, 0.03; 2.60, 0.73],[-116, 57; 76, -54])];
amp = amplifier_analysis(S);
assert(amp.absDelta - 0.487 < 1e-3);
assert(amp.K - 1.19 < 1e-2);
assert(amp.unconditionally_stable)

%% Pozar, Microwave Engineering 4th edition: Example 12.5
F_min = 1.6;
Gamma_opt = 0.62*exp(1j*(100)/180*pi);
R_N = 20;
S = [0.6*exp(1j*(-60)/180*pi),0.05*exp(1j*(26)/180*pi);1.9*exp(1j*(81)/180*pi),0.5*exp(1j*(60)/180*pi)];
amp = amplifier_analysis(S, 50, 50, F_min, Gamma_opt, R_N);

%% DTU 31420 Exam 2015
F_min = 1.17;
Gamma_opt = 0.47*exp(1j*(112.9)/180*pi);
R_N = 6.9;
Z_S = gamma2z(0.571*exp(1j*(132.5)/180*pi));
S = [0.725*exp(1j*(-143.2)/180*pi),0.098*exp(1j*(16.3)/180*pi);3.345*exp(1j*(74.4)/180*pi),0.389*exp(1j*(-67.1)/180*pi)];
amp_1 = amplifier_analysis(S, Z_S, 50, F_min, Gamma_opt, R_N);
assert(amp_1.F-1.3489 < 1e-3);
assert(abs(conj(amp_1.Gamma_out)-pdeg2c(0.551, 100.8)) < 1e-3);
% [a,d]=c2pdeg(amp_1.Gamma_out)

Z_L = gamma2z(conj(amp_1.Gamma_out));
amp_2 = amplifier_analysis(S, Z_S, Z_L, F_min, Gamma_opt, R_N);


%% DTU 31420 Exam 2014
F_min = 1.313;
Gamma_opt = 0.505*exp(1j*(72.9)/180*pi);
R_N = 0.032*50;
Z_S = gamma2z(0.44*exp(1j*(142.8)/180*pi));
S = [0.825*exp(1j*(-142.8)/180*pi),0.070*exp(1j*(20.0)/180*pi);7.069*exp(1j*(85.6)/180*pi),0.394*exp(1j*(173.0)/180*pi)];
amp_1 = amplifier_analysis(S, Z_S, 50, F_min, Gamma_opt, R_N);
Z_L = gamma2z(conj(S(2,2)));
amp_2 = amplifier_analysis(S, Z_S, Z_L, F_min, Gamma_opt, R_N);

