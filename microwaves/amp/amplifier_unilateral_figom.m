function [ U ] = amplifier_unilateral_figom( S )
%amplifier_unilateral_figom Summary of this function goes here
%   Detailed explanation goes here
    
    U = (abs(S(1,2))*abs(S(2,1))*abs(S(1,1))*abs(S(2,2)))/...
        ((1-abs(S(1,1))^2)*(1-abs(S(2,2))^2));

end

