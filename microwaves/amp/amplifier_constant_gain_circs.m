function [ constant_gain_c, constant_gain_r ] = amplifier_constant_gain_circs( gains, s )
%amplifier_constant_gain_circs Calculate constant gain circles of an amplifier.
%   Claclulaters centers and radiuses of constant gain circles of an
%   amplifier on source or load side depending on if s is S11 or S22 for
%   given vector or martix of gains.
    
    g_max = 1/(1-abs(s)^2);
    g = 10.^(gains./10)./(g_max);

    constant_gain_c = g.*conj(s)./(1-(1-g).*abs(s)^2);
    constant_gain_r = sqrt(1-g).*(1-abs(s)^2)./(1-(1-g).*abs(s)^2);

end
