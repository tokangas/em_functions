function [ constant_noise_c, constant_noise_r ] = amplifier_constant_noise_circs( nf, nf_min, R_N, Gamma_opt, Z_0, absolute_nf )
%amplifier_constant_noise_circs Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 6
        absolute_nf = 0;
    end
    if nargin < 5
        Z_0 = 50;
    end
    
    if ~absolute_nf
        F = 10.^(nf./10);
        F_min = 10.^(nf_min./10);
    end
    
    N = (F-F_min)./(4.*R_N./Z_0).*abs(1+Gamma_opt).^2;
    
    constant_noise_c = Gamma_opt./(N+1);
    constant_noise_r = sqrt(N.*(N+1-abs(Gamma_opt).^2))./(N+1);

end

