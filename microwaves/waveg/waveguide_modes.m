function [ k_c, f_c ] = waveguide_modes( mnl, abc )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    
    em = em_constants();
    
    abc = repmat(abc, [size(mnl , 1) 1]);
    
    k_c = sqrt(sum((pi*mnl./abc).^2,2));
    f_c = em.c_0*k_c/(2*pi);
end

