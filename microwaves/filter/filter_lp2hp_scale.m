function [ L_o, C_o ] = filter_lp2hp_scale( R_0, f, L_i, C_i )
%filter_lp2hp_scale Summary of this function goes here
%   Detailed explanation goes here
    omega = 2*pi.*f;
    
    L_o = R_0./C_i./omega;
    C_o = 1./L_i./R_0./omega;

end
