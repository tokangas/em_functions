function [ L_o_ser, C_o_ser, L_o_shu, C_o_shu ] = filter_lp2bp_scale( R_0, f, Delta, L_i, C_i )
%filter_lp2lp_scale Summary of this function goes here
%   Detailed explanation goes here
    omega = 2*pi.*f;
    
%     L_o = R_0.*L_i./omega;
%     C_o = C_i./R_0./omega;
    
    L_o_ser = R_0.*L_i./Delta./omega;
    C_o_ser = Delta./R_0./L_i./omega;
    
    L_o_shu = Delta.*R_0./C_i./omega;
    C_o_shu = C_i./Delta./omega./R_0;
    
end

