function [ L_o_par, C_o_par, L_o_ser, C_o_ser ] = filter_lp2bs_scale( R_0, f, Delta, L_i, C_i )
%filter_lp2lp_scale Summary of this function goes here
%   Detailed explanation goes here
    omega = 2*pi.*f;
    
%     L_o = R_0.*L_i./omega;
%     C_o = C_i./R_0./omega;
    
    L_o_par = Delta.*R_0.*L_i./omega;
    C_o_par = 1./R_0./L_i./Delta./omega;
    
    L_o_ser = 1./R_0./C_i./Delta./omega;
    C_o_ser = Delta.*R_0.*C_i./omega;
    
end

