function [ L_o, C_o ] = filter_lp2lp_scale( R_0, f, L_i, C_i )
%filter_lp2lp_scale Summary of this function goes here
%   Detailed explanation goes here
    omega = 2*pi.*f;
    
    L_o = R_0.*L_i./omega;
    C_o = C_i./R_0./omega;

end

