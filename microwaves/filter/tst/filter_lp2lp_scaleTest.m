%% filter_lp2lp_scale

%% Pozar, Microwave Engineering 4th edition: Example 8.3
[~,C] = filter_lp2lp_scale(50, 2e9, 0, 0.618);
assert(C-0.984e-12 < 1e-12);

[L,~] = filter_lp2lp_scale(50, 2e9, 1.618, 0);
assert(L-6.483e-9 < 1e-12);

[~,C] = filter_lp2lp_scale(50, 2e9, 2.000, 0);
assert(C-3.183e-12 < 1e-12);