%% filter_lp2bp_scale

%% Pozar, Microwave Engineering 4th edition: Example 8.4

[L_1,C_1,L_2,C_2] = filter_lp2bp_scale(50, 1e9, 0.1, 1.5963, 1.0967);
assert(L_1-127e-9 < 1e-9);
assert(C_1-0.199e-12 < 1e-15);
assert(L_2-0.726e-9 < 1e-12);
assert(C_2-34.91e-12 < 1e-14);
