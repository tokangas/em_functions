function [ R_o, L_o, C_o ] = tline_qwave_trans_RLC( R_i, L_i, C_i, Z_0 )
%tline_qwave_trans Series circuit to shunt circuit.
%   Detailed explanation goes here
    if nargin < 4
        Z_0 = 50;
    end
    
    % Y = R/Z_0^2 + i w L_i/Z_0^2 + (i w C*Z_0^2)^-1
    %       R              C               L
    
    R_o = Z_0^2./R_i;
    C_o = L_i./Z_0^2;
    L_o = C_i.*Z_0^2;
end

