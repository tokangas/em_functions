function [ Z_o ] = tline_qwave_trans( Z_i, Z_0 )
%tline_qwave_trans Series circuit to shunt circuit.
%   Detailed explanation goes here
    if nargin < 2
        Z_0 = 50;
    end
    
    Z_o = Z_0^2./Z_i;
end

