function [ epsilon_r, loss_tan, Z_0 ] = tline_le_unmodel( f, R, L, G, C )
%tline_le_unmodel Calculte substrate parameters of given tline model.
%   Detailed explanation goes here
    wave = wave_variables(f);
    
    gamma = sqrt((R+1j*wave.omega*L)*(G+1j*wave.omega*C));
    k = -1j*gamma;
    
    epsilon_r = real((k/wave.k_0)^2);
    loss_tan = -imag((k/wave.k_0)^2)/epsilon_r;
    
    Z_0 = sqrt((R+2j*pi*f*L)/(G+1j*wave.omega*C));
end

