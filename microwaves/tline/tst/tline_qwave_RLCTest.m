%% tline_qwave_RLC

%% DTU 31420 Exam 2013
J = 0.02;
f = 1e9;
omega = 2*pi*f;

C_1a = 45e-12;
L_1a = 562.7e-12;

[~,L_2b, C_2b] = tline_qwave_trans_RLC([], 112.5e-9,225.1e-15, 1/J);

C = J/omega;

C_1b = C_1a-C;
C_2b = C_2b-C;

assert(abs(C_1b-41.82e-12) < 1e-14);
assert(abs(C_2b-41.82e-12) < 1e-14);
assert(abs(L_2b-562.7e-12) < 1e-13);
