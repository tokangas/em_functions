function [ R, L, G, C ] = tline_le_model( f, epsilon_r, loss_tan, Z_0, lowloss )
%tline_le_model Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 5
        lowloss = 0;
    end
    if nargin < 4
        Z_0 = 50;
    end
    if nargin < 3
        loss_tan = 0;
    end
    
    if ~lowloss
        wave = wave_variables(f, epsilon_r*(1-1j*loss_tan));
    else
        wave = wave_variables(f, epsilon_r);
    end
    gamma = 1j*wave.k;

    R = real(Z_0*gamma);
    L = imag(Z_0*gamma)/wave.omega;

    G = real(gamma/Z_0);
    C = imag(gamma/Z_0)/wave.omega;
    
    if lowloss
        R = 0;
        G = wave.omega*C*loss_tan;
    end
    
end
