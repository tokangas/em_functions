function [ P_l ] = tline_p_delivered( k, l, V_g, Z_g, Z_l, Z_0 )
%tline_p_delivered Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 6
        Z_0 = 50;
    end
    
    Gamma_l = z2gamma(Z_l, Z_0);
    Gamma_g = z2gamma(Z_g, Z_0);
    
    V_0_plus = V_g*exp(-1j*k*l)/2*(1-Gamma_g)/...
        (1-Gamma_g*Gamma_l*exp(-2j*k*l));
    
    P_l = abs(V_0_plus)^2/(2*Z_0)*(1-abs(Gamma_l)^2);
end

