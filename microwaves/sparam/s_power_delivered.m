function [ P_load, P_input ] = s_power_delivered( S, P_a )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        P_a = 1;
    end
    
    P_input = P_a*(1-abs(S(1,1))^2);
    
    P_load = P_a*abs(S(2,1))^2;
    
end

