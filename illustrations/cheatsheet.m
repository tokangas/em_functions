%% EM Cheat Sheet, plots
% Toni Kangas
% toni.kangas@aalto.fi

%% Init env
clear
close all

%% Set variables
n = 200; % Number of points on plots
m = 6; % Number of curves per plot

z = linspace(0, 25, n);

%% Plot and print Bessel's functions
figure
subplot(2,1,1)
hold on
desc = [];
for nu = 0:(m-1)
    desc = [desc; 'J_', num2str(nu), '(x)'];
    plot(z,besselj(nu, z))
end
grid on
title('\it J_\nu(x)')
legend(desc)
subplot(2,1,2)
hold on
desc = [];
for nu = 0:(m-1)
    desc = [desc; 'Y_', num2str(nu), '(x)'];
    plot(z,bessely(nu, z))
end
grid on
title('\it Y_\nu(x)')
legend(desc)
ylim([-1,1])
hold off
print('img/bessel_j_y','-depsc')

%% Plot and print spherical Bessel's functions
figure
subplot(2,1,1)
hold on
desc = [];
for nu = 0:(m-1)
    desc = [desc; 'j_', num2str(nu), '(x)'];
    plot(z,sqrt(pi./(2.*z)).*besselj(nu+0.5, z))
end
grid on
title('\it j_\nu(x)')
legend(desc)
subplot(2,1,2)
hold on
desc = [];
for nu = 0:(m-1)
    desc = [desc; 'y_', num2str(nu), '(x)'];
    plot(z,sqrt(pi./(2.*z)).*bessely(nu+0.5, z))
end
grid on
title('\it y_\nu(x)')
legend(desc)
ylim([-1,1])
hold off
print('img/sph_bessel_j_y','-depsc')

%% Plot and print Legendre's functions
z_l = linspace(-1,1,n);
figure
subplot(2,1,1)
hold on
desc = [];
for nu = 0:3
    desc = [desc; 'P_', num2str(nu), '(x)'];
end
plot(z_l, 0*z_l+1);
plot(z_l, z_l);
plot(z_l, 1/2*(3.*z_l.^2-1));
plot(z_l, 1/2*(5.*z_l.^3-3*z_l));
plot(z_l, 1/8*(35.*z_l.^4-30*z_l.^2+3));
grid on
title('\it P_n(x)')
legend(desc)
subplot(2,1,2)
hold on
desc = [];
for nu = 0:3
    desc = [desc; 'Q_', num2str(nu), '(x)'];
end
plot(z_l, 1/2*log((1+z_l)./(1-z_l)))
plot(z_l, z_l./2.*log((1+z_l)./(1-z_l))-1)
plot(z_l, (3*z_l.^2-1)./4.*log((1+z_l)./(1-z_l))-3*z_l./2)
plot(z_l, (5*z_l.^3-3*z_l)./4.*log((1+z_l)./(1-z_l))-5*z_l.^2./2+2/3)
grid on
title('\it Q_n(x)')
legend(desc)
ylim([-1,1])
hold off
print('img/legendre_p_q','-depsc')

%% Plot and print perp refl and trans coeffs
figure
subplot(2,1,1)
hold on
desc = [];
theta_i = linspace(0,pi/2, n);
for r = [64 32 16 8 4 2]
    eta1 = 1;
    eta2 = 1/sqrt(r);
    theta_t = asin(1/sqrt(r)*sin(theta_i));
    desc = [desc; '\epsilon_2/\epsilon_1 = ',sprintf('%3d',r)];
    Gamma = (eta2*cos(theta_i)-eta1*cos(theta_t))./(eta2*cos(theta_i)+eta1*cos(theta_t));
    plot(theta_i/pi*180,abs(Gamma))
end
clear r
grid on
title('\it |\Gamma_\perp|')
legend(desc, 'location', 'west')
subplot(2,1,2)
hold on
desc = [];
for r = [64 32 16 8 4 2]
    eta1 = 1;
    eta2 = 1/sqrt(r);
    theta_t = asin(1/sqrt(r)*sin(theta_i));
    desc = [desc; '\epsilon_2/\epsilon_1 = ',sprintf('%3d',r)];
    T = (2*eta2*cos(theta_i))./(eta2*cos(theta_i)+eta1*cos(theta_t));
    plot(theta_i/pi*180,abs(T))
end
grid on
title('\it |T_\perp|')
legend(desc, 'location', 'west')
hold off
print('img/perp_gamma_t','-depsc')

%% Plot and print parallel refl and trans coeffs
figure
subplot(2,1,1)
hold on
desc = [];
theta_i = linspace(0,pi/2, n);
for r = [64 32 16 8 4 2]
    eta1 = 1;
    eta2 = 1/sqrt(r);
    theta_t = asin(1/sqrt(r)*sin(theta_i));
    desc = [desc; '\epsilon_2/\epsilon_1 = ',sprintf('%3d',r)];
    Gamma = (eta2*cos(theta_t)-eta1*cos(theta_i))./(eta2*cos(theta_t)+eta1*cos(theta_i));
    plot(theta_i/pi*180,abs(Gamma))
end
clear r
grid on
title('\it |\Gamma_{||}|')
legend(desc, 'location', 'west')
subplot(2,1,2)
hold on
desc = [];
for r = [64 32 16 8 4 2]
    eta1 = 1;
    eta2 = 1/sqrt(r);
    theta_t = asin(1/sqrt(r)*sin(theta_i));
    desc = [desc; '\epsilon_2/\epsilon_1 = ',sprintf('%3d',r)];
    T = (2*eta2*cos(theta_i))./(eta1*cos(theta_i)+eta2*cos(theta_t));
    plot(theta_i/pi*180,abs(T))
end
grid on
title('\it |T_{||}|')
legend(desc, 'location', 'west')
hold off
print('img/para_gamma_t','-depsc')

%% Plot binomial and chebyshev filters
% f_r = linspace(0, 2, n);
% figure
% subplot(2,1,1)
% hold on
% desc = [];
% for N = 2.^(0:m)
%     theta = pi/2*f_r;
%     gamma = 0.707*exp(-1j*N*theta).*cos(theta).^N;
%     desc = [desc; 'N = ',sprintf('%3d',N)];
%     plot(f_r, abs(gamma))
% end
% title('Binomial filter')
% legend(desc, 'location','north')
% xlabel('f/f_0')
% f_r = linspace(0.75, 1.25, n);
% subplot(2,1,2)
% hold on
% desc = [];
% theta_max = pi/2000;
% for N = 2.^(0:m)
%     theta = pi/2*f_r;
%     gamma = 0.707*exp(-1j*N*theta).*(chebyshevT(N,cos(theta)./cos(theta_max)))./(chebyshevT(N,1./cos(theta_max)));
%     desc = [desc; 'N = ',sprintf('%3d',N)];
%     plot(f_r, abs(gamma))
% end
% title('Chebyshev filter')
% legend(desc, 'location','north')
% xlabel('f/f_0')
