% Add this and subfolders to Matlab path
addpath(genpath(pwd));

tests = runtests(pwd,'IncludeSubfolders',true);
